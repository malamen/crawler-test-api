import textwrap
import json
from bs4 import BeautifulSoup
import requests
import sys


from django.http import HttpResponse
from django.views.generic.base import View
from django.views.decorators.csrf import csrf_exempt                                          


class RealHome(View):

    def dispatch(request, *args, **kwargs):
        response_text = textwrap.dedent('''\
            <html>
            <head>
                <title>Test crawl</title>
                <style>

					.node {
					  cursor: pointer;
					}

					.node circle {
					  fill: #fff;
					  stroke: steelblue;
					  stroke-width: 1.5px;
					}

					.node text {
					  font: 10px sans-serif;
					}

					.link {
					  fill: none;
					  stroke: #ccc;
					  stroke-width: 1.5px;
					}

				</style>
                <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        		<script src="http://d3js.org/d3.v3.min.js"></script>



		        <script>


		        	

					function update(source) {

				  // Compute the new tree layout.
				  var nodes = tree.nodes(root).reverse(),
				      links = tree.links(nodes);

				  // Normalize for fixed-depth.
				  nodes.forEach(function(d) { d.y = d.depth * 180; });

				  // Update the nodes…
				  var node = svg.selectAll("g.node")
				      .data(nodes, function(d) { return d.id || (d.id = ++i); });

				  // Enter any new nodes at the parent's previous position.
				  var nodeEnter = node.enter().append("g")
				      .attr("class", "node")
				      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
				      .on("click", click);

				  nodeEnter.append("circle")
				      .attr("r", 1e-6)
				      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

				  nodeEnter.append("text")
				      .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
				      .attr("dy", ".35em")
				      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
				      .text(function(d) { return d.url; })
				      .style("fill-opacity", 1e-6);

				  // Transition nodes to their new position.
				  var nodeUpdate = node.transition()
				      .duration(duration)
				      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

				  nodeUpdate.select("circle")
				      .attr("r", 4.5)
				      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

				  nodeUpdate.select("text")
				      .style("fill-opacity", 1);

				  // Transition exiting nodes to the parent's new position.
				  var nodeExit = node.exit().transition()
				      .duration(duration)
				      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
				      .remove();

				  nodeExit.select("circle")
				      .attr("r", 1e-6);

				  nodeExit.select("text")
				      .style("fill-opacity", 1e-6);

				  // Update the links…
				  var link = svg.selectAll("path.link")
				      .data(links, function(d) { return d.target.id; });

				  // Enter any new links at the parent's previous position.
				  link.enter().insert("path", "g")
				      .attr("class", "link")
				      .attr("d", function(d) {
				        var o = {x: source.x0, y: source.y0};
				        return diagonal({source: o, target: o});
				      });

				  // Transition links to their new position.
				  link.transition()
				      .duration(duration)
				      .attr("d", diagonal);

				  // Transition exiting nodes to the parent's new position.
				  link.exit().transition()
				      .duration(duration)
				      .attr("d", function(d) {
				        var o = {x: source.x, y: source.y};
				        return diagonal({source: o, target: o});
				      })
				      .remove();

				  // Stash the old positions for transition.
				  nodes.forEach(function(d) {
				    d.x0 = d.x;
				    d.y0 = d.y;
				  });
				}

				function click(d) {
				  if (d.children) {
				    d._children = d.children;
				    d.children = null;
				  } else {
				    d.children = d._children;
				    d._children = null;
				  }
				  update(d);
				}
				var root
				 function collapse(d) {
									    if (d.children) {
									      d._children = d.children;
									      d._children.forEach(collapse);
									      d.children = null;
									    }
									  }

						function myfunction(){
							var url = $(".website").val()
						    $.ajax ({
							    url: "/getsitemap",
							    type: "POST",
							    data: JSON.stringify({url:url}),
							    dataType: "json",
							    contentType: "application/json; charset=utf-8",
							    success: function(data){
						        	console.log(data)
						        	

									  root = data;
									  root.x0 = height / 2;
									  root.y0 = 0;

									 

									  root.children.forEach(collapse);
									  update(root);
									
						        }
						    });
						};

						</script>
            </head>
            <body>
            	<p>please add http:// or https://</p>
            	<p>This process can take some minutes :)</p>
				  Website: <input class="website" type="text" name="url"><br>
				  <input class="send_web" type="submit" value="Submit" onclick="myfunction()">
				</form>
				<div class="svg"></div>

            </body>
            <script>var margin = {top: 20, right: 120, bottom: 20, left: 120},
					    width = 960 - margin.right - margin.left,
					    height = 800 - margin.top - margin.bottom;

					var i = 0,
					    duration = 750,
					    root;

					var tree = d3.layout.tree()
					    .size([height, width]);

					var diagonal = d3.svg.diagonal()
					    .projection(function(d) { return [d.y, d.x]; });

					var svg = d3.select("body").append("svg")
					    .attr("width", width + margin.right + margin.left)
					    .attr("height", height + margin.top + margin.bottom)
					  .append("g")
					    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

					d3.select(self.frameElement).style("height", "800px");</script>

            </html>
        ''')
        return HttpResponse(response_text)

class HomePageView(View):
	
	@csrf_exempt                                                                                  
	def dispatch(request, *args, **kwargs):
		print(request.request.body.decode())
		url = json.loads(request.request.body.decode())['url']
		print(url)
		if not url[len(url)-1]=='/':
			url = url + "/"
		print("Using url: "+url)

		
		ret = node(url, url, "root").run()

		return HttpResponse(ret)

def recursive_node_to_dict(nod):
	"""create a dict from a tree"""

	result = {
		'url': nod.url,
		'title': nod.name
		}
	if len(nod.children)>0:
		children = []
		for c in nod.children:
			children.append(recursive_node_to_dict(c))
		result['children'] = children
	return result

def find(x, root):
	"""Performs find if exist node with the same url in the tree"""
	if root.url == x:
		return True
	for nod in root.children:
		if find(x, nod):
			return True
	return False

class node:

	""" Class node

    Class to keep n-tree of links inside of the website

    Attributes:
        children: Array of nodes that born from this node.
        url: URL of this node
        name: title of this website
        domain: url root
        json: json generated from this node
    """

	children = []
	url = ""
	name = ""
	domain= ""
	json = ""

	def __init__(self, url, domain, title):
		"""Inits nodeClass with localURLm DomainURLm and title of node."""
		self.url = url
		self.domain = domain
		self.name = title
		self.children = []
		self.json = ""

	def run(self):
		"""Performs operation to get tree and generate json in the node"""
		print("This process can take some minutes :)")
		self.find_children(self)
		self.to_json()
		#with open('out.json', 'w') as outfile:
		#	json.dump(self.json, outfile)
		return self.json

	def add_child(self, child_url, domain, title):
		"""Performs operation of add a new child to children array."""
		child = node(child_url, domain, title)
		self.children.append(child)

	def find_children(self, root):

		"""Performs recursive operation get all links check 
		if they are inside the domain and if they not exist in the tree,
		this fucntio is recursive in all children of this node"""

		headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
		content = requests.get(self.url, headers=headers).text
		soup = BeautifulSoup(content, "html.parser")
		links = soup.find_all("a", href=True)
		#print(self.url)
		title = soup.find("title").text
		#print(title)

		for link in links:
			url = link['href']
			if not "#" in url:
				if not url.startswith("mailto") and not url.startswith("http") and not url.startswith("www."):
					if url[0] == '/':
						url = url[1:]
					url = self.domain + url
					if self.domain in url:
						if not find(url, root):
							self.add_child(url,self.domain, title)
		for child in self.children:
			child.find_children(root)
	
	def to_json(self):
		"""Performs operation of generate json in the same node"""
		self.json = json.dumps(recursive_node_to_dict(self))




	