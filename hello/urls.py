from django.conf.urls import patterns, url

from hello.views import HomePageView, RealHome

urlpatterns = patterns(
    '',

    url(r'^$', RealHome.as_view(), name='home'),
    url(r'^getsitemap$', HomePageView.as_view(), name='getsitemap'),
)