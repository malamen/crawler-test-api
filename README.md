# README #

Fast crawler API using beautiful soup

* execution: python3 manage.py runserver
* endpoint: 52.168.141.28:8000/getsitemap
* data viz (beta) 52.168.141.28:8000
#### POST ####
json: {
	"url":"http://www.website.com/"
}

### What is this repository for? ###

* SimpleAPI crawler that return json with tree with sitemap
* Version(v.0.0.1)
* based on https://bitbucket.org/malamen/crawler-test

### How do I get set up? ###

pip3 install -r requirements.txt

### What is next? ###

* Data Visualization -> Fix